const formRequest = async e => {
    e.preventDefault();

    const header = { 
        'Accept':'application/json',
        'Content-Type': 'application/json'
    };

    const form = document.getElementById('subject-form');

    const jsonData = {
        'name': form.elements['name'].value
    };

    const response = await fetch('/new-subject', { method: "POST", headers: header, body: JSON.stringify(jsonData) });

    const data = await response.json();

    const myModal = document.getElementById('modalResponse');
    
    document.getElementById('msgResponse').innerHTML = data.msg;
    document.getElementById('titleResponse').innerHTML = data.title;

    myModal.classList.add(data.modalClass);
    myModal.classList.remove("hide");

    form.reset();
}

const editRequest = async e => {
    e.preventDefault();

    const header = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    };

    const form = document.getElementById('edit-form');

    const jsonData = {
        'name' : form.elements['name'].value,
        'old': form.elements['old'].value
    }

    const response = await fetch('/edit-subject', { method: 'POST', headers: header, body: JSON.stringify(jsonData)});

    const data = await response.json();

    const myModal = document.getElementById('modalResponse');
    
    document.getElementById('msgResponse').innerHTML = data.msg;
    document.getElementById('titleResponse').innerHTML = data.title;

    myModal.classList.add(data.modalClass);
    myModal.classList.remove("hide");

    console.log(response.ok);
}

const closeAlert = () => document.getElementById('modalResponse').classList.add("hide");

const deleteSubject = async name => {
    const header = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    };

    const jsonData = {
        "name" : name
    }

    const response = await fetch('/delete-subject', { method: 'POST', headers: header, body: JSON.stringify(jsonData)});

    const data = await response.json();

    const myModal = document.getElementById('modalResponse');
    
    document.getElementById('msgResponse').innerHTML = data.msg;
    document.getElementById('titleResponse').innerHTML = data.title;

    myModal.classList.add(data.modalClass);
    myModal.classList.remove("hide");

    setInterval(
        () => {
            window.location.reload(false);
        }
        ,3000);
}

const testDB = async (opt) => {
    const header = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    };

    const max = document.getElementById("max");

    const jsonData = {
        "max" : ( max !== null) ? parseInt(max.value) : 0,
        "opt" : parseInt(opt)
    }
    console.log(typeof opt, " val ", opt);

    const response = await fetch('/test-subject', { method: 'POST', headers: header, body: JSON.stringify(jsonData)});

    console.log(response.ok);
}