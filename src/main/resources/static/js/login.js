$(document).ready(function () {
    //Envió del correo  y contraseña, incluye lógica de los intentos
    $('#submit').click(function () {
        /* JSON.stringify({
            'mail': $('#email').val(),
            'pwd': $('#password-field').val()
        }) */
        $('#text-modal').text("");
        employee = $('#email').val();
        $.ajax({
            url: "/employee",
            //contentType: "application/json; charset=utf-8;",
            type: "POST",
            //dataType: "JSON",
            data:{employee: employee},
            success: function (data) {
                console.log(data);
                /* if (data['status'] === 'success') {
                    console.log('Correcto');
                    $("#myModal").modal();
                }
                else if (data['status'] === 'error') {
                    //Muestra la alerta de error
                    toggleAlert("Usuario y/o contraseña incorrecto.");
                    console.log('Fallo');

                    if (data['attempts'] >= 3) {
                        $("#change-password").removeClass('display: none');
                        $("#change-password").addClass('btn');
                        $("#change-password").addClass('btn-link');
                    }
                } */
            }
        });
    });


    //Modal, lógica del código de autenticación para login
    $('#submit-modal').click(function () {
        $.ajax({
            url: "/auth-code",
            type: "POST",
            dataType: "json",
            data: $('#auth-code').val(),
            success: function (data) {
                if (data['state'] === "success") {
                    //window.location.href = "login.html";
                    $('#modal-form').submit();
                } else {
                    toggleAlert("Código erroneo.");
                    console.log("Error código");
                }
            }
        });
    });


    //Reenviar código al correo
    $('#resend').click(function () {
        $.ajax({
            url: "/resend-mail",
            type: "POST",
            dataType: "json",
            data: $('#email').val(),
            success: function (data) {
                console.log(data);
                $('#text-modal').text('Correo reenviado.')
            }
        });
    });


    //Mostrar/ocultar contraseña
    $(".toggle-password").click(function () {
        $(this).toggleClass("glyphicon-eye-open glyphicon-eye-close");
        var input = $($(this).attr("toggle"));

        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });


    //Lanza el modelo para autenticar cambio de contraseña
    $("#save").click(function () {
        $.ajax({
            url: "/resend-mail",
            type: "POST",
            dataType: "JSON",
            data: $('#email').val(),
            success: function (data) {
                $("#myModal").modal();
            }
        });
    });


    //Verifica el código para guardar la contraseña
    $("#confirm-pass").click(function () {
        $.ajax({
            url: "/change-password",
            contentType: "application/json;charset=utf-8",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({
                'mail': $('#email').val(),
                'pwd': $('#password-field').val(),
                'code': $('#auth-code').val()
            }),
            success: function (data) {
                if (data['state'] === "success") {
                    alert("Contraseña cambiada exitosamente.")
                    window.location.href = "/";
                } else {
                    toggleAlert("Código erroneo.");
                    console.log("Error código");
                }
            }
        });
    });
});


//Controlar alerta de error
function toggleAlert(msg) {
    $('#error-msg').text(msg);
    $('.alert').toggleClass('in out');
    $('#password').val('');
}

//  Mostrar/ocultar contraseña en javascript puro
/* function showpass() {
    let pass = document.getElementById("password");
    let picon = document.getElementById("picon");


    if (pass.type === 'password') {
        pass.type = 'text';
        picon.className = 'glyphicon glyphicon-eye-close';
    }
    else {
        pass.type = 'password';
        picon.className = 'glyphicon glyphicon-eye-open';
    }
} */