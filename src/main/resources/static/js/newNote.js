window.onload = async e => {   
    console.log("Cargando materias");

    const header = { 
        'Accept':'application/json',
        'Content-Type': 'application/json'
    };

    const response = await fetch('/get-subjects', { method: "POST", headers: header })

    const subjects = await response.json();

    const selectBox = document.getElementById('select');

    subjects.map(({ name }) => {
        console.log(name);
        // create option using DOM
        const newOption = document.createElement('option');
        const optionText = document.createTextNode(name);
        // set option text
        newOption.appendChild(optionText);
        // and option value
        newOption.setAttribute('value', name);

        selectBox.appendChild(newOption);
    });
};
