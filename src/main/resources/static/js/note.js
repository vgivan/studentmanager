const formRequest = async e => {
    e.preventDefault();

    const header = { 
        'Accept':'application/json',
        'Content-Type': 'application/json'
    };

    const form = document.getElementById('note-form');

    const jsonData = {
        'subject': form.elements['select'].value,
        'curp': form.elements['curp'].value,
        'score': form.elements['score'].value
    };

    const response = await fetch('/new-note', { method: "POST", headers: header, body: JSON.stringify(jsonData) });

    const data = await response.json();

    const myModal = document.getElementById('modalResponse');
    
    document.getElementById('msgResponse').innerHTML = data.msg;
    document.getElementById('titleResponse').innerHTML = data.title;

    myModal.classList.add(data.modalClass);
    myModal.classList.remove("hide");

    //form.reset();
}



const editRequest = async e => {
    e.preventDefault();

    const header = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    };

    const form = document.getElementById('edit-form');

    const jsonData = {
        'subject': form.elements['subject'].value,
        'curp': form.elements['curp'].value,
        'score': form.elements['score'].value,
        'noteId': form.elements['noteId'].value
    };


    const response = await fetch('/edit-note', { method: 'POST', headers: header, body: JSON.stringify(jsonData)});

    const data = await response.json();

    const myModal = document.getElementById('modalResponse');
    
    document.getElementById('msgResponse').innerHTML = data.msg;
    document.getElementById('titleResponse').innerHTML = data.title;

    myModal.classList.add(data.modalClass);
    myModal.classList.remove("hide");

    console.log(response.ok);
}

const closeAlert = () => document.getElementById('modalResponse').classList.add("hide");

const deleteNote = async noteId => {
    const header = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    };

    const jsonData = {
        "id" : noteId
    }

    const response = await fetch('/delete-note', { method: 'POST', headers: header, body: JSON.stringify(jsonData)});

    const data = await response.json();

    const myModal = document.getElementById('modalResponse');
    
    document.getElementById('msgResponse').innerHTML = data.msg;
    document.getElementById('titleResponse').innerHTML = data.title;

    myModal.classList.add(data.modalClass);
    myModal.classList.remove("hide");

    setInterval(
        () => {
            window.location.reload(false);
        }
        ,3000);
}

const testDB = async (opt) => {
    const header = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    };

    const max = document.getElementById("max");

    const jsonData = {
        "max" : ( max !== null) ? parseInt(max.value) : 0,
        "opt" : parseInt(opt)
    }
    console.log(typeof opt, " val ", opt);

    const response = await fetch('/test-note', { method: 'POST', headers: header, body: JSON.stringify(jsonData)});

    console.log(response.ok);
}