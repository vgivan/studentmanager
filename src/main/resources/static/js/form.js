const formRequest = async e => {
    e.preventDefault();

    const header = { 
        'Accept':'application/json',
        'Content-Type': 'application/json'
    };

    const form = document.getElementById('student-form');

    const jsonData = {
        'curp': form.elements['curp'].value,
        'name' : form.elements['names'].value,
        'lastName': form.elements['apellido-01'].value, 
        'motherName': form.elements['apellido-02'].value,
        'age': form.elements['age'].value,  
        'state': form.elements['state'].value,      
        'level': form.elements['level'].value === '' && null,
        'group': form.elements['group'].value  === '' && null
    };

    const response = await fetch('/new-student', { method: "POST", headers: header, body: JSON.stringify(jsonData) });

    const data = await response.json();

    const myModal = document.getElementById('modalResponse');
    
    document.getElementById('msgResponse').innerHTML = data.msg;
    document.getElementById('titleResponse').innerHTML = data.title;

    myModal.classList.add(data.modalClass);
    myModal.classList.remove("hide");

    form.reset();
}

const editRequest = async e => {
    e.preventDefault();

    const header = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    };

    const form = document.getElementById('edit-form');

    const jsonData = {
        'curp': form.elements['curp'].value,
        'name' : form.elements['names'].value,
        'lastName': form.elements['apellido-01'].value,
        'motherName': form.elements['apellido-02'].value,
        'age': form.elements['age'].value,  
        'state': form.elements['state'].value,      
        'level': form.elements['level'].value === '' ? null : form.elements['level'].value,
        'group': form.elements['group'].value === '' ? null : form.elements['group'].value
    }

    console.log(JSON.stringify(jsonData));

    const response = await fetch('/edit-student', { method: 'POST', headers: header, body: JSON.stringify(jsonData)});

    const data = await response.json();

    const myModal = document.getElementById('modalResponse');
    
    document.getElementById('msgResponse').innerHTML = data.msg;
    document.getElementById('titleResponse').innerHTML = data.title;

    myModal.classList.add(data.modalClass);
    myModal.classList.remove("hide");

    console.log(response.ok);
}

const closeAlert = () => document.getElementById('modalResponse').classList.add("hide");

const deleteStudent = async curp => {
    const header = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    };

    const jsonData = {
        "curp" : curp   
    }

    const response = await fetch('/delete-student', { method: 'POST', headers: header, body: JSON.stringify(jsonData)});

    const data = await response.json();

    const myModal = document.getElementById('modalResponse');
    
    document.getElementById('msgResponse').innerHTML = data.msg;
    document.getElementById('titleResponse').innerHTML = data.title;

    myModal.classList.add(data.modalClass);
    myModal.classList.remove("hide");

    setInterval(
        () => {
            window.location.reload(false);
        }
        ,3000);
}

const testDB = async (opt) => {
    const header = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    };

    const max = document.getElementById("max");

    const jsonData = {
        "max" : ( max !== null) ? parseInt(max.value) : 0,
        "opt" : parseInt(opt)
    }
    console.log(typeof opt, " val ", opt);

    const response = await fetch('/test-student', { method: 'POST', headers: header, body: JSON.stringify(jsonData)});

    console.log(response.ok);
}