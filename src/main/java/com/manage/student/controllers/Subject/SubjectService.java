package com.manage.student.controllers.Subject;

import java.util.ArrayList;
import java.util.List;

import com.manage.models.Subject;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Servicio responsable de las consultas a la base de datos con hibernate
 */
@Service
public class SubjectService {
    private final static Logger LOG = LoggerFactory.getLogger(SubjectService.class);
    private final String HIBERNATE_CONFIG_FILE = "com/manage/hibernate.cfg.xml";

    /**
     * @return Lista de materias o null si no hay ninguna
     */
    public List<Subject> getSubjectList() {
        SessionFactory factorySession = new Configuration().configure(HIBERNATE_CONFIG_FILE)
                .addAnnotatedClass(Subject.class).buildSessionFactory();
        Session session = factorySession.openSession();

        try {
            session.beginTransaction();
            /**
             * NOTA: La consulta debe conincidir con el Modelo, no con la tabla de la base
             * de datos,
             * en este caso la tabla se llama Materias y se pone Subject como el modelo
             */
            String sqlQuery = String.format("from Subject");

            List<Subject> Subjects = new ArrayList<>();
            var query = session.createQuery(sqlQuery);

            Subjects = query.getResultList();

            if (Subjects.size() != 0) {
                LOG.info("Datos encontrados: " + Subjects.get(0).toString());
            } else {
                LOG.warn("Vacio :( ");
            }

            return Subjects;
        } catch (Exception ex) {
            LOG.error("Exeption get Subjects: " + ex);
            return null;
        } finally {
            session.close();
            factorySession.close();
            LOG.info("Cerrando conexión");
        }
    }
    

    /**
     * Agrega una nueva materia, siempre y cuando no este registrada ya
     * 
     * @param Subject
     * @return true si lo agregó correctamente y false si ocurre algun fallo
     */
    public boolean addSubject(Subject Subject) {
        SessionFactory factorySession = new Configuration().configure(HIBERNATE_CONFIG_FILE)
                .addAnnotatedClass(Subject.class).buildSessionFactory();
        Session session = factorySession.openSession();

        try {
            Transaction tx = session.beginTransaction();
            /**
             * Verifica si existe la materia antes de agregarlo
             */
            if (Subject != null) {
                if (((Subject) session.get(Subject.class, Subject.getName())) == null) {
                    session.flush();
                    session.save(Subject);
                    tx.commit();
                    return true;
                } else
                    return false;
            } else {
                LOG.info("Add Subject Subject is null");
                return false;
            }
        } catch (Exception ex) {
            LOG.error("Exeption add Subject: " + ex);
            return false;
        } finally {
            session.close();
            factorySession.close();
            LOG.info("Cerrando conexión");
        }
    }

    /**
     * Obtiene a la materia con su nombre
     * 
     * @param name
     * @return Objeto de tipo Subject si existe o null si no se encuentra la materia
     */
    public Subject getSubjectByName(String name) {
        SessionFactory factorySession = new Configuration().configure(HIBERNATE_CONFIG_FILE)
                .addAnnotatedClass(Subject.class).buildSessionFactory();
        Session session = factorySession.openSession();

        try {
            Transaction tx = session.beginTransaction();

            Subject Subject = (Subject) session.get(Subject.class, name);
            tx.commit();

            LOG.info("Subject name, " + (Subject == null ? "" : Subject.toString()));

            return (Subject != null) ? Subject : null;
        } catch (Exception ex) {
            LOG.error("Exeption get Subject by name: " + ex);
            return null;
        } finally {
            session.close();
            factorySession.close();
            LOG.info("Cerrando conexión");
        }
    }

    /**
     * Elimina una materia que coincida con el nombre
     * 
     * @param name
     * @return True si lo eliminó correctamente y false si no se encontra u ocurra
     *         algún error
     */
    public boolean deleteSubject(String name) {
        SessionFactory factorySession = new Configuration().configure(HIBERNATE_CONFIG_FILE)
                .addAnnotatedClass(Subject.class).buildSessionFactory();
        Session session = factorySession.openSession();

        try {
            Transaction tx = session.beginTransaction();

            Subject subject = (Subject) session.load(Subject.class, name);

            if (subject != null) {
                session.delete(subject);

                tx.commit();

                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            LOG.error("Exeption delete Subject: " + ex);
            return false;
        } finally {
            session.close();
            factorySession.close();
            LOG.info("Cerrando conexión");
        }
    }

    /**
     * Actualiza la información de una materia
     * 
     * @param Subject
     * @return True si se actualiza correctamente o false si ocurre algún fallo
     */
    public boolean updateSubject(String odlName, String name) {
        SessionFactory factorySession = new Configuration().configure(HIBERNATE_CONFIG_FILE)
                .addAnnotatedClass(Subject.class).buildSessionFactory();
        Session session = factorySession.openSession();

        try {
            Transaction tx = session.beginTransaction();
            String sqlQuery = String.format("update Subject set name='%s' where name='%s'", name, odlName);

            session.createQuery(sqlQuery).executeUpdate();

            tx.commit();

            return true;
        } catch (Exception ex) {
            LOG.error("Exeption update Subjtect: " + ex);
            return false;
        } finally {
            session.close();
            factorySession.close();
            LOG.info("Cerrando conexión");
        }
    }

    public boolean testSubjects() {
        Subject Subject = new Subject();

        String[] names = { "Español", "Matemáticas", "Ciencias Naturales", "Cívica y ética", "Historia" };

        for (int i = 0; i < names.length; i++) {
            Subject = new Subject(names[i]);

            this.addSubject(Subject);
        }

        return true;
    }

    public void cleanSubjects() {
        SessionFactory factorySession = new Configuration().configure(HIBERNATE_CONFIG_FILE)
                .addAnnotatedClass(Subject.class).buildSessionFactory();
        Session session = factorySession.openSession();

        try {
            session.beginTransaction();

            String sqlQuery = "DELETE FROM Subject";
            session.createQuery(sqlQuery).executeUpdate();
            LOG.info("DB ELIMINDADA");

            session.getTransaction().commit();
        } catch (Exception ex) {
            LOG.error("Exeption delete Subjects: " + ex);
        } finally {
            session.close();
            factorySession.close();
            LOG.info("Cerrando conexión");
        }
    }
}
