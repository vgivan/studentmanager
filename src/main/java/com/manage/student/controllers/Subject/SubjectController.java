package com.manage.student.controllers.Subject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.manage.models.Subject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class SubjectController {
    private final static Logger LOG = LoggerFactory.getLogger(SubjectController.class);
    
    @Autowired
    private SubjectService subjectService;


     /**
     * @brief Metodo que despliega una lista de materias
     * @param none
     * @return Lista de estudiantes
     */
    @GetMapping("/subjects")
    public String getSubjects(Model model) {
        List<Subject> subjects = subjectService.getSubjectList();
        model.addAttribute("subjects", subjects);

        return "subjects/subjects.html";
    }

    /**
     * Retorna un arreglo de materias
     * @return
     */
    @ResponseBody
    @PostMapping("/get-subjects")
    public List<Subject> getSubjetsJSON(){
        return subjectService.getSubjectList();
    }


     /**
     * Método para desplegar el formulario nueva materia, pidiendo la url de donde procede para el callback
     * @param model
     * @return Vista nuevo materia
     */
    @GetMapping("/new/subject")
    public String getNewSubject(Model model) {

        return "subjects/new-subject.html";
    }

    /**
     * Método para validar nueva materia, regresa un JSON 
     * @param newSubject
     * @param model
     * @return JSON ocn mensaje, clase y titulo para la respuesta
     */
    @ResponseBody
    @PostMapping("/new-subject")
    public Map<String, String> getNewSubject(@RequestBody Subject newSubject, Model model) {
        Map<String, String> map = new HashMap<String, String>();
        String msg = "", title = "", modalClass = "";

        if (subjectService.addSubject(newSubject)){ 
            title = "Creación exitosa!";
            modalClass = "alert-success";
            msg = "Materia creada con exito.";
        } else {
            title = "Error al crear!";
            modalClass = "alert-warning";
            msg = "Materia no creada, metaria ya resgistrada.";
        }

        map.put("title", title);
        map.put("modalClass", modalClass);
        map.put("msg", msg);        

        return map;        
    }


    /**
     * Método para desplegar vista de editar materia, recibe nombre de la materia a editar
     * @param nombre
     * @param model
     * @return Vista editar materia con los datos del materia cargado
     */
    @GetMapping("/edit/subject/{subjectName}")
    public String getsubjectEditData(@PathVariable String subjectName, Model model) {
        LOG.info(subjectName);
        
        Subject subject = subjectService.getSubjectByName(subjectName);
        
        LOG.info(subject.toString());
        
        model.addAttribute("subject", subject);
        
        return "subjects/edit-subject.html";
    }
    
    /**
     * Método para eliminar materia, recibe un JSON, con la información del materia
     * @param newsubject
     * @return JSON con título, mensaje y clase del alert
     */
    @ResponseBody
    @PostMapping("/edit-subject")
    public Map<String, String> editsubject(@RequestBody Map<String, String> request) {
        Map<String, String> map = new HashMap<String, String>();
        String msg = "", title = "", modalClass = "";

        if (subjectService.updateSubject(request.get("old"), request.get("name"))){ 
            title = "Actualización exitosa!";
            modalClass = "alert-success";
            msg = "Materia actualizada con exito.";
        } else {
            title = "Error al actulizar!";
            modalClass = "alert-warning";
            msg = "Materia no actualizada, error inesperado verifique los datos.";
        }

        map.put("title", title);
        map.put("modalClass", modalClass);
        map.put("msg", msg);        

        return map;
    }


    /**
     * Método para eliminar materia, recibe un JSON, con el nombre de la materia
     * @param request
     * @param model
     * @return JSON con título, mensaje y clase del alert
     */
    @ResponseBody
    @PostMapping("/delete-subject")
    public Map<String, String> deletesubject(@RequestBody Map<String, String> request, Model model) {
        String subjectName = request.get("name");
        LOG.info("subjectName a eliminar " + subjectName);

        Map<String, String> map = new HashMap<String, String>();
        String msg = "", title = "", modalClass = "";

        if (subjectName != null) {
            if (subjectService.deleteSubject(subjectName)){ 
                title = "Eliminación exitosa!";
                modalClass = "alert-success";
                msg = "Materia eliminada con exito.";
            } else {
                title = "Error al eliminar!";
                modalClass = "alert-warning";
                msg = "Materia no eliminada, materia no encontrado.";
            }
        } else {
            title = "Error Inesperado!";
            modalClass = "alert-danger";
            msg = "Materia no encontrado, verifique los datos.";
        }

        map.put("title", title);
        map.put("modalClass", modalClass);
        map.put("msg", msg);        

        return map;
    }
}
