package com.manage.student.controllers.Student;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.manage.models.Student;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;


/**
 * Servicio responsable de las consultas a la base de datos con hibernate
 */
@Service
public class StudentService {
    private final static Logger LOG = LoggerFactory.getLogger(StudentService.class);
    private final String HIBERNATE_CONFIG_FILE = "com/manage/hibernate.cfg.xml";

    /**
     * @return Lista de estudiantes registrados o null si no hay ningún estudiante
     */
    public List<Student> getStudentList() {
        SessionFactory factorySession = new Configuration().configure(HIBERNATE_CONFIG_FILE)
                .addAnnotatedClass(Student.class).buildSessionFactory();
        Session session = factorySession.openSession();

        try {
            session.beginTransaction();
            /**
             * NOTA: La consulta debe conincidir con el Modelo, no con la tabla de la base de datos, 
             * en este caso la tabla se llama Estudiantes y se pobe Student como el modelo
            */
            String sqlQuery = String.format("from Student");

            List<Student> students = new ArrayList<>();
            var query = session.createQuery(sqlQuery);

            students = query.getResultList();

            if(students.size() != 0){
                LOG.info("Datos encontrados: " + students.get(0).toString());    
            }
            else {
                LOG.warn("Vacio :( ");
            }

            return students;
        } catch (Exception ex) {
            LOG.error("Exeption get students: " + ex);
            return null;
        } finally {
            session.close();
            factorySession.close();
            LOG.info("Cerrando conexión");
        }
    }

    /**
     * Agrega un nuevo estudiante, siempre y cuando no este registrado ya
     * @param student
     * @return true si lo agregó correctamente y false si ocurre algun fallo
     */
    public boolean addStudent(Student student) {
        SessionFactory factorySession = new Configuration().configure(HIBERNATE_CONFIG_FILE)
                .addAnnotatedClass(Student.class).buildSessionFactory();
        Session session = factorySession.openSession();

        try {
            Transaction tx = session.beginTransaction(); 
            /**
             * Verifica si existe el estudiante antes de agregarlo
             */
            if(student != null){
                if (((Student) session.get(Student.class, student.getCurp())) == null) {
                    session.flush();
                    session.save(student);
                    tx.commit();    
                    return true;
                }
                else
                    return false;
            } else {
                LOG.info("Add student student is null");
                return false;
            }
        } catch (Exception ex) {
            LOG.error("Exeption add student: " + ex);
            return false;
        } finally {
            session.close();
            factorySession.close();
            LOG.info("Cerrando conexión");
        }
    }

    /**
     * Obtiene al estudiante con su curp
     * @param curp
     * @return Objeto de tipo Student si existe o null si no se encuentra el estudiante
     */
    public Student getStudentByCurp(String curp) {
        SessionFactory factorySession = new Configuration().configure(HIBERNATE_CONFIG_FILE)
                .addAnnotatedClass(Student.class).buildSessionFactory();
        Session session = factorySession.openSession();

        try {
            Transaction tx = session.beginTransaction(); 

            Student student = (Student) session.get(Student.class, curp);
            tx.commit();

            LOG.info("Student curp, " + (student == null ? "" : student.toString()));

            return (student != null) ? student : null;
        } catch (Exception ex) {
            LOG.error("Exeption get student by id: " + ex);
            return null;
        } finally {
            session.close();
            factorySession.close();
            LOG.info("Cerrando conexión");
        }
    }

    /**
     * Elimina un estudiante que coincida con el curp dado
     * @param curp
     * @return True si lo eliminó correctamente y false si no se encontra u ocurra algún error
     */
    public boolean deleteStudent(String curp) {
        SessionFactory factorySession = new Configuration().configure(HIBERNATE_CONFIG_FILE)
                .addAnnotatedClass(Student.class).buildSessionFactory();
        Session session = factorySession.openSession();

        try {
            Transaction tx = session.beginTransaction(); 

            Student student = (Student) session.load(Student.class, curp);

            if (student != null) {
                session.delete(student);

                tx.commit();

                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            LOG.error("Exeption delete student: " + ex);
            return false;
        } finally {
            session.close();
            factorySession.close();
            LOG.info("Cerrando conexión");
        }
    }

    /**
     * Actualiza la información de un estudiante
     * @param student
     * @return True si se actualiza correctamente o false si ocurre algún fallo
     */
    public boolean updateStudent(Student student) {
        SessionFactory factorySession = new Configuration().configure(HIBERNATE_CONFIG_FILE)
                .addAnnotatedClass(Student.class).buildSessionFactory();
        Session session = factorySession.openSession();

        try {
            Transaction tx = session.beginTransaction(); 
            
            LOG.info("Update student " + student.toString());

            session.update(student);

            tx.commit();

            return true;
        } catch (Exception ex) {
            LOG.error("Exeption update studet: " + ex);
            return false;
        } finally {
            session.close();
            factorySession.close();
            LOG.info("Cerrando conexión");
        }
    }

    public boolean testStudents(int max) {
        Student student = new Student();

        String[] states = { "Aguascalientes", "Baja California", "Baja California Sur", "Campeche", "Chiapas",
                            "Chihuahua", "Coahuila de Zaragoza", "Colima", "Durango", "Estado de México",
                            "Guanajuato", "Guerrero", "Hidalgo", "Jalisco", "Michoacán de Ocampo", "Morelos",
                            "Nayarit", "Nuevo León", "Oaxaca", "Puebla", "Querétaro", "Quintana Roo", 
                            "San Luis Potosí", "Sinaloa", "Sonora", "Tabasco", "Tamaulipas", "Tlaxcala",
                            "Veracruz de Ignacio de la Llave", "Yucatán", "Zacatecas" };

        String[] names = {"Sofía", "Valentina", "Regina", "María José", "Ximena", "Camila", 
                            "María Fernanda", "Valeria", "Victoria", "Renata", "Santiago", 
                            "Mateo", "Sebastián", "Leonardo", "Matías", "Emiliano", "Diego", 
                            "Daniel", "Miguel Ángel" ,"Alexander"};

        String[] lastnames = { "Hernández", "García", "Martínez", "López", "González", "Pérez", "Rodríguez",
                                "Sánchez", "Ramírez", "Cruz", "Flores", "Gómez", "Morales", "Vázquez",
                                "Reyes", "Jiménez", "Torres", "Díaz", "Gutiérrez", "Ruiz" };

        for (int i = 0; i < max; i++) {
            student = new Student(("CURP"+i), 
                                    names[new Random().nextInt(names.length)], 
                                    lastnames[new Random().nextInt(lastnames.length)], 
                                    lastnames[new Random().nextInt(lastnames.length)], 
                                    i, 
                                    states[new Random().nextInt(states.length)]);
                                    
            this.addStudent(student);
        }

        return true;
    }

    public void cleanStudents(){
        SessionFactory factorySession = new Configuration().configure(HIBERNATE_CONFIG_FILE)
                .addAnnotatedClass(Student.class).buildSessionFactory();
        Session session = factorySession.openSession();
        
        LOG.info("Entrando a clean");

        try {
            session.beginTransaction();

            String sqlQuery = "DELETE FROM Student";
            session.createQuery(sqlQuery).executeUpdate();
            LOG.info("DB ELIMINDADA");

            session.getTransaction().commit();
        } catch (Exception ex) {
            LOG.error("Exeption update studet: " + ex);
        } finally {
            session.close();
            factorySession.close();
            LOG.info("Cerrando conexión");
        }
    }
}
