/**
 * Controlador Estudiantes, aquí van las rutas de administrador de estudiantes
 * @author Ivan Vargas
 * @date Enero 7, 2022
 */

package com.manage.student.controllers.Student;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.manage.models.Student;
import com.manage.student.controllers.Note.NoteService;
import com.manage.student.controllers.Subject.SubjectService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class StudentController {
    private final static Logger LOG = LoggerFactory.getLogger(StudentController.class);

    @Autowired
    private StudentService studentService;

    @Autowired
    private SubjectService subjectService;

    @Autowired
    private NoteService noteService;


    /**
     * Método para las pruebas de consultas a la base de datos
     * @param request recibe un JSON con la opción a ejecutar
     * @return true
     */
    @ResponseBody
    @PostMapping("/test-student")
    public boolean testDB(@RequestBody Map<String, Integer> request) {
        int max = request.get("max");
        int opt = request.get("opt");

        switch (opt) {
            case 1:
                subjectService.testSubjects();
                studentService.testStudents(max);
                noteService.testNotes(studentService.getStudentList(), subjectService.getSubjectList());

                LOG.info("CASE 01: Example data created.");
                break;
            case 2:
                studentService.cleanStudents();
                subjectService.cleanSubjects();
                noteService.cleanNotes();

                LOG.info("CASE 02: DB data deleted.");
                break;
        
            default:
                LOG.warn("ERROR");
                break;
        }

        return true;
    }

    /**
     * @brief Metodo que despliega una lista de estudiantes
     * @param none
     * @return Lista de estudiantes
     */
    @GetMapping("/students")
    public String getEmployees(Model model) {
        List<Student> students = studentService.getStudentList();
        model.addAttribute("students", students);

        return "/students/students.html";
    }


    /**
     * Método para desplegar el formulario nuevo estudiante, pidiendo la url de donde procede para el callback
     * @param url
     * @param model
     * @return Vista nuevo estudiante
     */
    @GetMapping("/new/student/{url}")
    public String newStudent(@PathVariable String url, Model model) {

        model.addAttribute("url", "/" + url);
        LOG.info("url ", url);

        return "/students/new-student.html";
    }

    /**
     * Método para guardar al nuevo estudiante, recibe un JSON con los parametros de estudiante
     * @param newStudent
     * @param model
     * @return JSON, que contiene título, mensaje y clase de la alert
     */
    @ResponseBody
    @PostMapping("/new-student")
    public Map<String, String> validateUser(@RequestBody Student newStudent, Model model) {
        Map<String, String> map = new HashMap<String, String>();
        String msg = "", title = "", modalClass = "";

        if (studentService.addStudent(newStudent)){ 
            title = "Creación exitosa!";
            modalClass = "alert-success";
            msg = "Estudiante creado con exito.";
        } else {
            title = "Error!";
            modalClass = "alert-warning";
            msg = "Estudiante no creado, CURP ya resgistrado.";
        }

        map.put("title", title);
        map.put("modalClass", modalClass);
        map.put("msg", msg);        

        return map;
    }

    /**
     * Método para eliminar estudiante, recibe un JSON, con el curp del estudiante
     * @param request
     * @param model
     * @return JSON con título, mensaje y clase del alert
     */
    @ResponseBody
    @PostMapping("/delete-student")
    public Map<String, String> deleteStudent(@RequestBody Map<String, String> request, Model model) {
        String curp = request.get("curp");
        LOG.info("Curp a eliminar " + curp);

        Map<String, String> map = new HashMap<String, String>();
        String msg = "", title = "", modalClass = "";

        if (curp != null) {
            if (studentService.deleteStudent(curp)){ 
                title = "Eliminación exitosa!";
                modalClass = "alert-success";
                msg = "Estudiante eliminado con exito.";
            } else {
                title = "Error!";
                modalClass = "alert-warning";
                msg = "Estudiante no eliminado, estudiante no encontrado.";
            }
        } else {
            title = "Error Inesperado!";
            modalClass = "alert-danger";
            msg = "Estudiante no encontrado, verifique los datos.";
        }

        map.put("title", title);
        map.put("modalClass", modalClass);
        map.put("msg", msg);        

        return map;
    }
    
    /**
     * Método para desplegar vista de editar estudiante, recibe curp del estudiante a editar
     * @param curp
     * @param model
     * @return Vista editar estudiante con los datos del estudiante cargado
     */
    @GetMapping("/edit/student/{curp}")
    public String getStudentEditData(@PathVariable String curp, Model model) {
        LOG.info(curp);
        
        Student student = studentService.getStudentByCurp(curp);
        
        LOG.info(student.toString());
        
        model.addAttribute("student", student);
        
        return "/students/edit-student.html";
    }
    
    /**
     * Método para eliminar estudiante, recibe un JSON, con la información del estudiante
     * @param newStudent
     * @return JSON con título, mensaje y clase del alert
     */
    @ResponseBody
    @PostMapping("/edit-student")
    public Map<String, String> editStudent(@RequestBody Student newStudent) {
        LOG.info(newStudent.toString());

        Map<String, String> map = new HashMap<String, String>();
        String msg = "", title = "", modalClass = "";

        if (studentService.updateStudent(newStudent)){ 
            title = "Actualización exitosa!";
            modalClass = "alert-success";
            msg = "Estudiante actualizado con exito.";
        } else {
            title = "Error!";
            modalClass = "alert-warning";
            msg = "Estudiante no actualizado, verifique los datos.";
        }

        map.put("title", title);
        map.put("modalClass", modalClass);
        map.put("msg", msg);        

        return map;
    }

    /**
     * Método para obtener un estudiante conociendo su curp
     * @param student
     * @return JSON con título, mensaje y clase del alert
     */
    @ResponseBody
    @GetMapping("/get/student")
    public Student getStudent(@RequestBody Map<String,String> studentData) {
        String curp = studentData.get("curp");

        if (curp != null) {
            Student student = studentService.getStudentByCurp(curp);
            return ( student == null ) ? student : null;
        } else 
            return null;
    }
}
