package com.manage.student.controllers.Note;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.manage.models.Note;
import com.manage.models.Student;
import com.manage.student.controllers.Student.StudentService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class NoteController {
    private final static Logger LOG = LoggerFactory.getLogger(NoteController.class);
    
    @Autowired
    private NoteService noteService;

    @Autowired
    private StudentService studentService;


     /**
     * @brief Metodo que despliega una lista de todas calificaciones
     * @param none
     * @return Lista de calificaciones
     */
    @GetMapping("/notes")
    public String getNotes(Model model) {
        List<Note> notes = noteService.getAllNotes();

        model.addAttribute("notes", notes);

        return "notes/notes.html";
    }


     /**
     * Método para desplegar el formulario nueva calificación de un alumno
     * @param curp
     * @param model
     * @return Vista nueva Calificación
     */
    @GetMapping("/new/note/{curp}")
    public String getNewnote(@PathVariable String curp, Model model) {
        model.addAttribute("curp", curp);

        return "notes/new-note.html";
    }

    /**
     * Método para validar nueva calificación, regresa un JSON 
     * @param newNote
     * @param model
     * @return JSON ocn mensaje, clase y titulo para la respuesta
     */
    @ResponseBody
    @PostMapping("/new-note")
    public Map<String, String> getNewnote(@RequestBody Note newNote, Model model) {
        Map<String, String> map = new HashMap<String, String>();
        String msg = "", title = "", modalClass = "";

        if (noteService.addNote(newNote)){ 
            title = "Creación exitosa!";
            modalClass = "alert-success";
            msg = "Calificación creada con exito.";
        } else {
            title = "Error al crear!";
            modalClass = "alert-warning";
            msg = "Calificación no creada, metaria ya resgistrada.";
        }

        map.put("title", title);
        map.put("modalClass", modalClass);
        map.put("msg", msg);        

        return map;        
    }


    /**
     * Método para desplegar vista de editar Calificación, recibe nombre de la Calificación a editar
     * @param noteId
     * @param model
     * @return Vista editar Calificación con los datos de la Calificación cargado
     */
    @GetMapping("/edit/note/{noteId}")
    public String getnoteEditData(@PathVariable String noteId, Model model) {
        Note note = noteService.getNote(noteId);
        
        LOG.info(note.toString());
        
        model.addAttribute("note", note);
        
        return "notes/edit-note.html";
    }
    
    /**
     * Método para eliminar Calificación, recibe un JSON, con la información de la Calificación
     * @param newnote
     * @return JSON con título, mensaje y clase del alert
     */
    @ResponseBody
    @PostMapping("/edit-note")
    public Map<String, String> editnote(@RequestBody Note newNote) {
        Map<String, String> map = new HashMap<String, String>();
        String msg = "", title = "", modalClass = "";

        if (noteService.updateNote(newNote)){ 
            title = "Actualización exitosa!";
            modalClass = "alert-success";
            msg = "Calificación actualizada con exito.";
        } else {
            title = "Error al actulizar!";
            modalClass = "alert-warning";
            msg = "Calificación no actualizada, error inesperado verifique los datos.";
        }

        map.put("title", title);
        map.put("modalClass", modalClass);
        map.put("msg", msg);        

        return map;
    }


    /**
     * Método para eliminar Calificación, recibe un JSON, con el nombre de la Calificación
     * @param request
     * @param model
     * @return JSON con título, mensaje y clase del alert
     */
    @ResponseBody
    @PostMapping("/delete-note")
    public Map<String, String> deletenote(@RequestBody Map<String, String> request, Model model) {
        String noteId = request.get("id");
        LOG.info("noteId a eliminar " + noteId);

        Map<String, String> map = new HashMap<String, String>();
        String msg = "", title = "", modalClass = "";

        if (noteId != null) {
            if (noteService.deleteNote(Integer.parseInt(noteId))){ 
                title = "Eliminación exitosa!";
                modalClass = "alert-success";
                msg = "Calificación eliminada con exito.";
            } else {
                title = "Error al eliminar!";
                modalClass = "alert-warning";
                msg = "Calificación no eliminada, Calificación no encontrado.";
            }
        } else {
            title = "Error Inesperado!";
            modalClass = "alert-danger";
            msg = "Calificación no encontrado, verifique los datos.";
        }

        map.put("title", title);
        map.put("modalClass", modalClass);
        map.put("msg", msg);        

        return map;
    }
}
