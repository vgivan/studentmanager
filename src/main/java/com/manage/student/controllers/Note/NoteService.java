package com.manage.student.controllers.Note;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.manage.models.Note;
import com.manage.models.Student;
import com.manage.models.Subject;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Servicio responsable de las consultas a la base de datos con hibernate
 */
@Service
public class NoteService {
    private final static Logger LOG = LoggerFactory.getLogger(NoteService.class);
    private final String HIBERNATE_CONFIG_FILE = "com/manage/hibernate.cfg.xml";


    /**
     * @return Lista de Calificaciones registrados de un estudiante o null si no hay ningún estudiante
     * @param curp
     */
    public List<Note> getAllNotes() {
        SessionFactory factorySession = new Configuration().configure(HIBERNATE_CONFIG_FILE)
                .addAnnotatedClass(Note.class).buildSessionFactory();
        Session session = factorySession.openSession();

        try {
            session.beginTransaction();
            /**
             * NOTA: La consulta debe conincidir con el Modelo, no con la tabla de la base de datos, 
             * en este caso la tabla se llama Estudiantes y se pobe Note como el modelo
            */
            String sqlQuery = String.format("from Note");

            List<Note> notes = new ArrayList<>();
            var query = session.createQuery(sqlQuery);

            notes = query.getResultList();

            if(notes.size() != 0){
                LOG.info("Datos encontrados: " + notes.get(0).toString());    
                return notes;
            }
            else {
                LOG.warn("Vacio :( ");
                return null;
            }
        } catch (Exception ex) {
            LOG.error("Exeption get notes: " + ex);
            return null;
        } finally {
            session.close();
            factorySession.close();
            LOG.info("Cerrando conexión");
        }
    }



    /**
     * @return Lista de Calificaciones registrados de un estudiante o null si no hay ningún estudiante
     * @param curp
     */
    public List<Note> getStudentNotes(String curp) {
        SessionFactory factorySession = new Configuration().configure(HIBERNATE_CONFIG_FILE)
                .addAnnotatedClass(Note.class).buildSessionFactory();
        Session session = factorySession.openSession();

        try {
            session.beginTransaction();
            /**
             * NOTA: La consulta debe conincidir con el Modelo, no con la tabla de la base de datos, 
             * en este caso la tabla se llama Estudiantes y se pobe Note como el modelo
            */
            if(curp != null){
                String sqlQuery = String.format("from Note as nt where nt.curp = %s", curp);
    
                List<Note> notes = new ArrayList<>();
                var query = session.createQuery(sqlQuery);
    
                notes = query.getResultList();
    
                if(notes.size() != 0){
                    LOG.info("Datos encontrados: " + notes.get(0).toString());    
                }
                else {
                    LOG.warn("Vacio :( ");
                }
    
                return notes;
            } else {
                return null;
            }

        } catch (Exception ex) {
            LOG.error("Exeption get notes: " + ex);
            return null;
        } finally {
            session.close();
            factorySession.close();
            LOG.info("Cerrando conexión");
        }
    }

    /**
     * Agrega un calificacion recibiendo una Note
     * @param Note 
     * @return true si lo agregó correctamente y false si ocurre algun fallo
     */
    public boolean addNote(Note note) {
        SessionFactory factorySession = new Configuration().configure(HIBERNATE_CONFIG_FILE)
                .addAnnotatedClass(Note.class).buildSessionFactory();
        Session session = factorySession.openSession();

        try {
            Transaction tx = session.beginTransaction(); 
            /**
             * Verifica si existe el estudiante antes de agregarlo
             */
            if(note != null){
                if (((Note) session.get(Note.class, note.getScore())) == null) {
                    session.flush();
                    session.save(note);
                    tx.commit();    
                    return true;
                }
                else
                    return false;
            } else {
                LOG.info("Add Note Note is null");
                return false;
            }
        } catch (Exception ex) {
            LOG.error("Exeption add Note: " + ex);
            return false;
        } finally {
            session.close();
            factorySession.close();
            LOG.info("Cerrando conexión");
        }
    }

    /**
     * Devuelve la nota de una materia de un alumno
     * @param curp
     * @param subject
     * @return
     */
    public Note getStudentSubjectNote(String curp, String subject) {
        SessionFactory factorySession = new Configuration().configure(HIBERNATE_CONFIG_FILE)
                .addAnnotatedClass(Note.class).buildSessionFactory();
        Session session = factorySession.openSession();

        try {
            session.beginTransaction();
            /**
             * NOTA: La consulta debe conincidir con el Modelo, no con la tabla de la base de datos, 
             * en este caso la tabla se llama Estudiantes y se pobe Note como el modelo
            */
            if (subject != null) {
                String sqlQuery = String.format("from Note as nt where nt.subject = %s and nt.curp = %s", subject, curp);
    
                List<Note> notes = new ArrayList<>();
                var query = session.createQuery(sqlQuery);
    
                notes = query.getResultList();
    
                if(notes.size() != 0){
                    LOG.info("Datos encontrados: " + notes.get(0).toString());    
                }
                else {
                    LOG.warn("Vacio :( ");
                }
    
                return notes.get(0);
            } else {
                return null;
            }
        } catch (Exception ex) {
            LOG.error("Exeption get notes: " + ex);
            return null;
        } finally {
            session.close();
            factorySession.close();
            LOG.info("Cerrando conexión");
        }
    }


    public Note getNote(String noteId) {
        SessionFactory factorySession = new Configuration().configure(HIBERNATE_CONFIG_FILE)
                .addAnnotatedClass(Note.class).buildSessionFactory();
        Session session = factorySession.openSession();

        try {
            session.beginTransaction();
            /**
             * NOTA: La consulta debe conincidir con el Modelo, no con la tabla de la base de datos, 
             * en este caso la tabla se llama Estudiantes y se pobe Note como el modelo
            */
            if (noteId != null) {
                String sqlQuery = String.format("from Note as nt where nt.noteId = %s", noteId);
    
                List<Note> notes = new ArrayList<>();
                var query = session.createQuery(sqlQuery);
    
                notes = query.getResultList();
    
                if(notes.size() != 0){
                    LOG.info("Datos encontrados: " + notes.get(0).toString());    
                }
                else {
                    LOG.warn("Vacio :( ");
                }
    
                return notes.get(0);
            } else {
                return null;
            }
        } catch (Exception ex) {
            LOG.error("Exeption get notes: " + ex);
            return null;
        } finally {
            session.close();
            factorySession.close();
            LOG.info("Cerrando conexión");
        }
    }


    /**
     * @return Lista de todas las Calificaciones registrados de una materia o null si no hay ningún estudiante
     * @param subject
     */
    public List<Note> getSubjectNotes(String subject) {
        SessionFactory factorySession = new Configuration().configure(HIBERNATE_CONFIG_FILE)
                .addAnnotatedClass(Note.class).buildSessionFactory();
        Session session = factorySession.openSession();

        try {
            session.beginTransaction();
            /**
             * NOTA: La consulta debe conincidir con el Modelo, no con la tabla de la base de datos, 
             * en este caso la tabla se llama Estudiantes y se pobe Note como el modelo
            */
            if (subject != null) {
                String sqlQuery = String.format("from Note as nt where nt.subject = %s", subject);
    
                List<Note> Notes = new ArrayList<>();
                var query = session.createQuery(sqlQuery);
    
                Notes = query.getResultList();
    
                if(Notes.size() != 0){
                    LOG.info("Datos encontrados: " + Notes.get(0).toString());    
                }
                else {
                    LOG.warn("Vacio :( ");
                }
    
                return Notes;
            } else {
                return null;
            }
        } catch (Exception ex) {
            LOG.error("Exeption get Notes: " + ex);
            return null;
        } finally {
            session.close();
            factorySession.close();
            LOG.info("Cerrando conexión");
        }
    }

    /**
     * Elimina una calificación que coincida con la notaId
     * @param noteId
     * @return True si lo eliminó correctamente y false si no se encontra u ocurra algún error
     */
    public boolean deleteNote(int noteId) {
        SessionFactory factorySession = new Configuration().configure(HIBERNATE_CONFIG_FILE)
                .addAnnotatedClass(Note.class).buildSessionFactory();
        Session session = factorySession.openSession();

        try {
            Transaction tx = session.beginTransaction();

            Note note = (Note) session.load(Note.class, noteId);

            if (note != null) {
                session.delete(note);

                tx.commit();

                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            LOG.error("Exeption delete Note: " + ex);
            return false;
        } finally {
            session.close();
            factorySession.close();
            LOG.info("Cerrando conexión");
        }
    }

    /**
     * Actualiza la información de una calificación
     * @param note
     * @return True si se actualiza correctamente o false si ocurre algún fallo
     */
    public boolean updateNote(Note note) {
        SessionFactory factorySession = new Configuration().configure(HIBERNATE_CONFIG_FILE)
                .addAnnotatedClass(Note.class).buildSessionFactory();
        Session session = factorySession.openSession();

        try {
            Transaction tx = session.beginTransaction(); 
            
            if(note != null){
                session.update(note);

                tx.commit();

                return true;
            } else {
                LOG.error("Nota null");
                return false;
            }
        } catch (Exception ex) {
            LOG.error("Exeption update note: " + ex);
            return false;
        } finally {
            session.close();
            factorySession.close();
            LOG.info("Cerrando conexión");
        }
    }

    public boolean testNotes(List<Student> students, List<Subject> subjects) {
        Note Note = new Note();

        int max = (students.size() >= subjects.size()) ? subjects.size() : students.size();

        for (int i = 0; i < max; i++) {
            Note = new Note(students.get(i).getCurp(), subjects.get(i).getName(), new Random().nextInt(10));
                                    
            this.addNote(Note);
        }

        return true;
    }

    public void cleanNotes(){
        SessionFactory factorySession = new Configuration().configure(HIBERNATE_CONFIG_FILE)
                .addAnnotatedClass(Note.class).buildSessionFactory();
        Session session = factorySession.openSession();

        try {
            session.beginTransaction();

            String sqlQuery = "DELETE FROM Note";
            session.createQuery(sqlQuery).executeUpdate();
            LOG.info("DB ELIMINDADA");

            session.getTransaction().commit();
        } catch (Exception ex) {
            LOG.error("Exeption update studet: " + ex);
        } finally {
            session.close();
            factorySession.close();
            LOG.info("Cerrando conexión");
        }
    }
}
