package com.manage.student;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * brief
 */
@SpringBootApplication
public class ManageApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(ManageApplication.class, args);
	}

}
