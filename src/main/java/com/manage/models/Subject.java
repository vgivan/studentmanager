package com.manage.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Materias")
public class Subject {
    @Id
    @Column(name = "Nombre")
    private String name;


    /**
     * Constructor con id y nombre de la materia
     * @param name
     */
    public Subject(String name) {
        this.name = name;
    }

    /**
     * Constructor vacio
     */
    public Subject() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Subject [name=" + name + "]";
    }
}  
