package com.manage.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Calificaciones")
public class Note {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_mat")
    private int noteId;

    @Column(name = "CURP")
    private String curp;

    @Column(name = "Materia")
    private String subject;

    @Column(name = "Calificacion")
    private int score;


    /**
     * Constructor con los parametros:
     * @param noteId
     * @param curp
     * @param subject
     * @param score
     */
    public Note(String curp, String subject, int score) {
        this.curp = curp;
        this.subject = subject;
        this.score = score;
    }

    /**
     * Constructor vacio
     */
    public Note(){

    }

    public int getNoteId(){
        return noteId;
    }

    public int getIdMat() {
        return noteId;
    }

    public void setIdMat(int idNote) {
        this.noteId = idNote;
    }

    public String getCurp() {
        return curp;
    }

    public void setCurp(String curp) {
        this.curp = curp;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return "Note [curp=" + curp + ", id=" + subject + ", idMat=" + noteId + ", score=" + score + "]";
    }

    
}
