/**
 * Modelo estudiante para manipular la base de datos a través de Hibernate
 * @author Ivan Vargas
 * @date Enero 8, 2022 
 */
package com.manage.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Estudiantes")
public class Student {
    @Id
    @Column(name = "CURP")
    private String curp;

    @Column(name = "Nombre")
    private String name;

    @Column(name = "ApellidoPaterno")
    private String lastName;

    @Column(name = "ApellidoMaterno")
    private String motherName;

    @Column(name = "Edad")
    private int age;

    @Column(name = "Estado")
    private String state;

    @Column(name = "Grado")
    private String level;

    @Column(name = "Grupo")
    private String group;

    
    public Student() {

    }

    
    /**
     *Constructor con todos los parámetro requeridos
     * @param curp
     * @param name
     * @param lastName
     * @param motherName
     * @param age
     * @param state
     * @param level
     * @param group
     */
    public Student(String curp, String name, String lastName, String motherName, int age, String state, String level, String group) {
        this.curp = curp;
        this.name = name;
        this.lastName = lastName;
        this.motherName = motherName;
        this.age = age;
        this.state = state;
        this.level = level;
        this.group = group;
    }


    /**
     * Contructor sin necesidad de grupo y grado
     * @param curp
     * @param name
     * @param lastName
     * @param motherName
     * @param age
     * @param state
     */
    public Student(String curp, String name, String lastName, String motherName, int age, String state) {
        this.curp = curp;
        this.name = name;
        this.lastName = lastName;
        this.motherName = motherName;
        this.age = age;
        this.state = state;
    }


    public String getCurp() {
        return curp;
    }


    public void setCurp(String curp) {
        this.curp = curp;
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public String getLastName() {
        return lastName;
    }


    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    public String getMotherName() {
        return motherName;
    }
    

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    @Override
    public String toString() {
        return "Student [age=" + age + ", curp=" + curp + ", group=" + group + ", lastName=" + lastName + ", motherName=" + motherName + ", level="
                + level + ", name=" + name + ", state=" + state + "]";
    }    
}
