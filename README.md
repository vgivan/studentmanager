Sistema para el registro de estudiantes, materias y calificaciones siguiendo la guía de estilos gob.mx creadó en Spring haciendo uso de Hibernate para el manejo de las consultas a la base de datos.

**Configuración**

*Se recomienda el uso de VS Code y tener instalada la extension de Spring Boot Extension Pack https://marketplace.visualstudio.com/items?itemName=Pivotal.vscode-boot-dev-pack*

1. Clonar el repositorio
2. Abrirlo en VS Code
3. En la ventana **SPRING BOOT DASHBOARD** dar click en **Start...**
4. En la ventana principal se tiene un campo para generar datos de prueba, se introduce la cantidad de alumnos que deseé generar y presione el boton **test**, esto también generará registros en materias y calificaiones.

<br  />

> Nota: El archivo Student es la base de datos en SQLite3, **no eliminar.**

<br />

---

<br />

## Agregar nuevas rutas

Las rutas son manejadas por los controladores, si pertenecen a una de las categorias ya creadas(Estudiante, Materia o Calificación) solo añadir en el respectivo archivo, si no crear el controlador en su respectiva carpeta.

Ejmplo:     DepartmentController.java

Consideraciones

1. Los controladores llevan la nota especial ***@Controller***
2. Si requiere consultas a la base de datos separa la logica en un archivo aparte al nivel del controlador.

<br />

---

<br />

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.
